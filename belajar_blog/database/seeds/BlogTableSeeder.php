<?php

use Illuminate\Database\Seeder;

class BlogTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('blog')->insert([
        	'title' => '7 Makanan Tradisional Khas Jawa yang Cocok Disajikan saat HUT RI ke-75.',
        	'content' => 'Jakarta - Makanan tradisional khas Jawa pilihannya sangat beragam 
        	semua makanan ini memilikimakna spesialsehingga jadi sajian populer saat 
        	perayaan-perayaan tertentu, termasuk pada Hari Kemerdekaan RI yang ke-75 ini.',
        	'category' => 'HUT RI ke-75'
    ]);
    }
}
