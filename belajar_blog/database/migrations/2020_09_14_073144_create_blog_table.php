<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
             Schema::create('blog', function (Blueprint $table) {
             //Auto Incremet, integer, primary key
            $table->increments('id');
            $table->string('title',150);
            $table->text('content');
            //Membuat Field created_at_update_at
            $table->timestamps();
            //membuat Field deleted_at
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog');
    }
}
