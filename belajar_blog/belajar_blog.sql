-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 16, 2020 at 01:15 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `belajar_blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `title`, `content`, `category`, `created_at`, `updated_at`, `deleted_at`) VALUES
(5, '7 Makanan Tradisional Khas Jawa yang Cocok Disajikan saat HUT RI ke-75.', 'Jakarta - Makanan tradisional khas Jawa pilihannya sangat beragam \n        	semua makanan ini memilikimakna spesialsehingga jadi sajian populer saat \n        	perayaan-perayaan tertentu, termasuk pada Hari Kemerdekaan RI yang ke-75 ini.', 'HUT RI ke-75', NULL, NULL, NULL),
(6, 'test', 'test', 'test', NULL, NULL, NULL),
(7, 'Ms.', 'Et voluptatum itaque nisi quia. Unde distinctio possimus repellat voluptas quam ducimus.', 'Woodworking Machine Setter', '2020-09-14 02:28:00', '2020-09-14 02:28:00', NULL),
(8, 'Ms.', 'Consequuntur inventore ut dolorem. Rem delectus libero magni ut perferendis sunt.', 'Construction Equipment Operator', '2020-09-14 02:28:00', '2020-09-14 02:28:00', NULL),
(9, 'Mr.', 'Sint qui corporis unde velit aut quis voluptas perspiciatis. Neque ad ut dolorem illo culpa eos sint sit. Et sunt inventore perspiciatis distinctio minima aut accusantium.', 'Tire Changer', '2020-09-14 02:28:00', '2020-09-14 02:28:00', NULL),
(10, 'Dr.', 'Qui in mollitia nostrum maxime. Blanditiis cum velit maxime quae exercitationem. Atque qui ea voluptatem non.', 'Museum Conservator', '2020-09-14 02:28:00', '2020-09-14 02:28:00', NULL),
(11, 'Mr.', 'Sed qui nisi inventore. Quia quas enim alias ut. Ipsum rerum cumque recusandae deserunt. Quo illo error hic. Omnis provident sit ut officia.', 'Protective Service Worker', '2020-09-14 02:28:00', '2020-09-14 02:28:00', NULL),
(12, 'Miss', 'Officiis labore aliquid beatae voluptatem omnis odio delectus quis. Numquam dignissimos et neque quisquam. Est nisi eligendi fugiat consequatur perspiciatis pariatur quae nobis. Recusandae ratione molestiae eligendi dolor hic.', 'Commercial Diver', '2020-09-14 02:28:00', '2020-09-14 02:28:00', NULL),
(13, 'Prof.', 'Ut in cumque voluptatem et impedit. Nihil tempora voluptatibus possimus nam qui recusandae dolorem commodi. Beatae qui hic dolores iusto reprehenderit dignissimos quis. Voluptatem hic corrupti incidunt quo. Natus ea omnis doloremque quaerat modi.', 'Title Examiner', '2020-09-14 02:28:00', '2020-09-14 02:28:00', NULL),
(14, 'Ms.', 'Quis ullam odit incidunt quia voluptatem. Tempore corrupti qui dolorem. Voluptatem repellat qui est quasi non et.', 'Sys Admin', '2020-09-14 02:28:00', '2020-09-14 02:28:00', NULL),
(15, 'Miss', 'Natus et et culpa. Quae sunt aut dignissimos earum. Accusantium nesciunt voluptatem fugiat quia aperiam facere molestiae minus. Quaerat eos voluptate et et quia earum. Accusantium inventore dolorem ea accusantium ea.', 'Insurance Appraiser', '2020-09-14 02:28:00', '2020-09-14 02:28:00', NULL),
(16, 'Ms.', 'Autem ipsa rem repellat. Omnis dolorem non veniam voluptatem alias id sed neque. Sit ipsam et porro.', 'Environmental Compliance Inspector', '2020-09-14 02:28:00', '2020-09-14 02:28:00', NULL),
(17, 'Dr.', 'Modi ut nam pariatur ullam dolores. Laboriosam ullam voluptas porro distinctio corrupti itaque. Qui velit enim perspiciatis aut consectetur.', 'Advertising Manager OR Promotions Manager', '2020-09-14 02:28:00', '2020-09-14 02:28:00', NULL),
(18, 'Mr.', 'Non explicabo dolorum quis voluptas. Nobis voluptatem optio quia eum doloremque aut non. Et ipsa fuga sunt quas aliquid perspiciatis soluta. Maxime quis nam consequatur et rem vero enim.', 'Etcher and Engraver', '2020-09-14 02:28:01', '2020-09-14 02:28:01', NULL),
(19, 'Dr.', 'Velit sunt voluptatem odio facere sed facilis. Similique quam ut quos ad. Sint enim laboriosam minus delectus libero blanditiis.', 'Data Entry Operator', '2020-09-14 02:28:01', '2020-09-14 02:28:01', NULL),
(20, 'Mr.', 'Nostrum pariatur at ipsa. Veritatis impedit hic ipsam ut.', 'Tax Preparer', '2020-09-14 02:28:01', '2020-09-14 02:28:01', NULL),
(21, 'Dr.', 'Delectus nisi iste perspiciatis. Et ipsa harum culpa rem. Atque sint fugiat ut aut mollitia. Et quos at dolores veritatis tenetur.', 'Industrial Engineering Technician', '2020-09-14 02:28:01', '2020-09-14 02:28:01', NULL),
(22, 'Ms.', 'Maiores a dicta iste itaque sit. Facere voluptatem culpa at voluptate. Recusandae distinctio officiis beatae quasi in. Ex earum asperiores eum quam. Cumque possimus nostrum et consequatur illum assumenda quo.', 'Costume Attendant', '2020-09-14 02:28:01', '2020-09-14 02:28:01', NULL),
(23, 'Mrs.', 'Non dicta quae animi autem. Quae consequatur magni sed mollitia. Quas reiciendis omnis labore beatae. Numquam voluptatum incidunt rem nihil recusandae et. Commodi eum illum non velit voluptatem blanditiis non laudantium.', 'Plumber OR Pipefitter OR Steamfitter', '2020-09-14 02:28:01', '2020-09-14 02:28:01', NULL),
(24, 'Mrs.', 'Aut exercitationem et odio quis sunt. Accusantium non qui inventore nam cum pariatur architecto aut. Hic expedita voluptatum reprehenderit accusamus.', 'Business Manager', '2020-09-14 02:28:01', '2020-09-14 02:28:01', NULL),
(25, 'Mrs.', 'Ipsam ad facilis quae necessitatibus architecto. Doloremque quibusdam assumenda dolorem doloribus facere. Accusantium omnis beatae iste sapiente fuga veniam. Ab quaerat dolore enim.', 'Air Crew Member', '2020-09-14 02:28:01', '2020-09-14 02:28:01', NULL),
(26, 'Mr.', 'Rerum odio nobis est pariatur. Quas sit eum nisi labore deserunt cum et sint. Et quos et ut maxime nihil.', 'Tree Trimmer', '2020-09-14 02:28:01', '2020-09-14 02:28:01', NULL),
(27, 'Dr.', 'Aut autem molestiae impedit. Voluptatem qui doloremque officia non. Voluptatibus eligendi voluptate voluptatem illo odit expedita natus. Quos nostrum laboriosam ullam dicta et.', 'Mathematical Scientist', '2020-09-14 02:28:01', '2020-09-14 02:28:01', NULL),
(28, 'Ms.', 'Qui occaecati ut aliquam nesciunt ullam iusto. Nam at pariatur facere eligendi dolorum. Est inventore aperiam temporibus.', 'Sawing Machine Operator', '2020-09-14 02:28:01', '2020-09-14 02:28:01', NULL),
(29, 'Prof.', 'Et est repellendus velit et quis maxime est. Sit minima omnis iste ea. Quo accusantium earum tempora necessitatibus. Enim maiores rerum eos minima laudantium ea et.', 'Media and Communication Worker', '2020-09-14 02:28:01', '2020-09-14 02:28:01', NULL),
(30, 'Prof.', 'Dolores odit dolor sit est ut. Aut est excepturi eum sed nihil. Ratione aut expedita dolorem odio quaerat enim sunt et. Inventore pariatur eaque blanditiis dolorem iste odit.', 'Home Appliance Repairer', '2020-09-14 02:28:01', '2020-09-14 02:28:01', NULL),
(31, 'Mr.', 'Repudiandae quis est eos est saepe. Laboriosam consequatur odio perferendis dolor. In dignissimos enim illum voluptas dolores. Nulla ullam ab illum natus harum qui ad.', 'Mail Clerk', '2020-09-14 02:28:01', '2020-09-14 02:28:01', NULL),
(32, 'Prof.', 'Omnis qui et consequatur enim cum. Nostrum saepe dolor quia. Eveniet sint expedita enim dicta aut aliquam.', 'Market Research Analyst', '2020-09-14 02:28:01', '2020-09-14 02:28:01', NULL),
(33, 'Miss', 'Omnis ut iusto beatae voluptatem. Perferendis eius qui rerum quas dolore ad nesciunt. Sunt adipisci expedita quia cupiditate ut. Atque veritatis aliquid qui.', 'File Clerk', '2020-09-14 02:28:01', '2020-09-14 02:28:01', NULL),
(34, 'Prof.', 'Ut sunt perspiciatis delectus repellendus. Dolorem non quisquam voluptas. Delectus ipsa in officia animi minima voluptate consequatur.', 'Heavy Equipment Mechanic', '2020-09-14 02:28:01', '2020-09-14 02:28:01', NULL),
(35, 'Dr.', 'Eius et perferendis aut nemo saepe et est magnam. Eos aut perferendis minima vero. Officia eligendi dolor eum fugiat sapiente neque quos. Quae magnam repellendus a qui ea.', 'Middle School Teacher', '2020-09-14 02:28:01', '2020-09-14 02:28:01', NULL),
(36, 'Prof.', 'Qui labore mollitia saepe aperiam nihil non tempore. Voluptates incidunt ex repellat quisquam. Qui nisi saepe accusantium. Dicta cumque et minus.', 'Spraying Machine Operator', '2020-09-14 02:28:01', '2020-09-14 02:28:01', NULL),
(37, 'Dr.', 'Sint et aut autem earum. Dolorem sunt et rem sunt molestiae in molestias. Et quam totam voluptatem blanditiis labore. Impedit earum quod illum nam aut.', 'Landscape Artist', '2020-09-14 02:28:01', '2020-09-14 02:28:01', NULL),
(38, 'Prof.', 'Magnam ipsum ad incidunt quia. Rerum voluptatem eum distinctio sed. Voluptatem omnis non qui voluptas nam. Quos eveniet sit eos velit.', 'Financial Manager', '2020-09-14 02:28:01', '2020-09-14 02:28:01', NULL),
(39, 'Dr.', 'Tempora consequatur necessitatibus suscipit earum qui. Delectus veritatis aut et eligendi nulla.', 'Epidemiologist', '2020-09-14 02:28:01', '2020-09-14 02:28:01', NULL),
(40, 'Mrs.', 'Voluptas et suscipit est tempora quam. Tempora maxime ratione explicabo architecto aperiam voluptatum voluptas. Laboriosam recusandae sed veniam nihil dignissimos saepe totam non.', 'Buyer', '2020-09-14 02:28:01', '2020-09-14 02:28:01', NULL),
(41, 'Miss', 'Assumenda tenetur consequatur hic et. Qui beatae provident aliquam qui quos.', 'Dot Etcher', '2020-09-14 02:28:01', '2020-09-14 02:28:01', NULL),
(42, 'Mr.', 'Quo molestiae voluptas non eos facilis et ut quo. Deleniti atque a et a assumenda dicta qui. Suscipit dolor et minima sunt aut et amet.', 'Medical Records Technician', '2020-09-14 02:28:01', '2020-09-14 02:28:01', NULL),
(43, 'Dr.', 'Dicta voluptatem quis doloribus quaerat architecto. Necessitatibus dolore voluptatem saepe quia consequuntur quae. Sit quam tempore est illum voluptas dolorum. In quisquam exercitationem consequatur provident quas qui.', 'Postsecondary Teacher', '2020-09-14 02:28:01', '2020-09-14 02:28:01', NULL),
(44, 'Ms.', 'Optio fugit nisi deleniti cum voluptatibus incidunt voluptatem dignissimos. Commodi est ut optio excepturi quibusdam magni ut. Quo explicabo temporibus non ut dolores.', 'Aircraft Launch Specialist', '2020-09-14 02:28:02', '2020-09-14 02:28:02', NULL),
(45, 'Miss', 'Natus a et accusantium aut. Cumque fuga totam laudantium voluptates quidem deleniti. Atque sed quod consectetur et. Aliquid consequatur accusantium sit perferendis amet.', 'Biological Scientist', '2020-09-14 02:28:02', '2020-09-14 02:28:02', NULL),
(46, 'Prof.', 'Corporis et deserunt aut maxime sint optio exercitationem eligendi. Est ipsa consequatur sed omnis occaecati sed. Velit cum molestias aut rerum in. Omnis aliquid omnis quos qui.', 'Tool Set-Up Operator', '2020-09-14 02:28:02', '2020-09-14 02:28:02', NULL),
(47, 'Prof.', 'Quos consequatur omnis doloribus ut accusantium. Fugiat temporibus quidem minima quia numquam autem ipsa. Aperiam delectus autem et.', 'Architectural Drafter OR Civil Drafter', '2020-09-14 02:28:02', '2020-09-14 02:28:02', NULL),
(48, 'Miss', 'Et quod laudantium occaecati quas. Ut voluptatibus iusto debitis nostrum vero autem necessitatibus. Necessitatibus voluptatem nisi modi a quas. Voluptas quidem rerum voluptatem et beatae natus.', 'Credit Authorizer', '2020-09-14 02:28:02', '2020-09-14 02:28:02', NULL),
(49, 'Miss', 'Ea aperiam quia qui non. Totam rerum sed iste sequi.', 'Fire-Prevention Engineer', '2020-09-14 02:28:02', '2020-09-14 02:28:02', NULL),
(50, 'Prof.', 'Sit voluptatum doloribus maxime odit delectus illum quis. Officiis exercitationem quo architecto impedit. Dignissimos id eum dolorum voluptates. Officiis aliquam provident voluptatem itaque molestias. Eum quidem praesentium et nesciunt excepturi occaecati.', 'Head Nurse', '2020-09-14 02:28:02', '2020-09-14 02:28:02', NULL),
(51, 'Mr.', 'Et consequatur similique velit maxime. Aspernatur aut facilis provident vitae tempora. Magnam sint voluptates explicabo sint non error. Dicta et mollitia et minima odio quia voluptas.', 'Head Nurse', '2020-09-14 02:28:02', '2020-09-14 02:28:02', NULL),
(52, 'Mr.', 'Nostrum ex praesentium delectus quidem placeat maiores numquam. Quia et veniam voluptatem voluptas sint vel. Possimus et qui veritatis ea. Officia voluptatem quisquam ipsum velit.', 'Watch Repairer', '2020-09-14 02:28:02', '2020-09-14 02:28:02', NULL),
(53, 'Dr.', 'Explicabo magnam et exercitationem qui. Perferendis et non aut vel ex rerum. Ut rerum vel quo ut aliquid repellat numquam.', 'Steel Worker', '2020-09-14 02:28:02', '2020-09-14 02:28:02', NULL),
(54, 'Dr.', 'Nobis voluptatem quod accusantium numquam. Voluptatibus blanditiis molestiae vel delectus unde ut consectetur. Ab laudantium non molestiae suscipit. Facilis modi tempore culpa ea beatae voluptate quam nulla.', 'Painting Machine Operator', '2020-09-14 02:28:02', '2020-09-14 02:28:02', NULL),
(55, 'Prof.', 'Magnam dolor aliquid quo autem. Sed officia sint eum harum magnam voluptates ab. Dolores repellendus optio accusantium cumque.', 'Architect', '2020-09-14 02:28:02', '2020-09-14 02:28:02', NULL),
(56, 'Prof.', 'Libero nostrum ut aut non corporis corrupti odit quibusdam. Et voluptas quos facilis quod. Fuga fugit qui eveniet sunt.', 'School Social Worker', '2020-09-14 02:28:02', '2020-09-14 02:28:02', NULL),
(57, 'Prof.', 'Ea molestiae pariatur temporibus sit quo exercitationem. Cum consequuntur quaerat accusantium doloremque facilis et. Reiciendis accusamus distinctio aspernatur odio porro quia.', 'Team Assembler', '2020-09-14 02:28:02', '2020-09-14 02:28:02', NULL),
(58, 'Miss', 'Ut iste et fuga est sint laboriosam. Quia enim fugit molestiae inventore. Est voluptatem autem sapiente beatae illum voluptate. Consequatur ipsa sint voluptas consequatur tempora omnis fugit exercitationem.', 'Webmaster', '2020-09-14 02:28:02', '2020-09-14 02:28:02', NULL),
(59, 'Mr.', 'Aut autem ab molestiae quaerat repudiandae eos minus sapiente. Dolores voluptatem aspernatur id. Provident expedita quaerat unde sit soluta optio sit iure.', 'Mapping Technician', '2020-09-14 02:28:02', '2020-09-14 02:28:02', NULL),
(60, 'Mr.', 'Repellat consequuntur tenetur voluptatem voluptatem voluptatum vero. Fuga recusandae ut ad cumque. Inventore deserunt quasi nihil ut odio.', 'Coaches and Scout', '2020-09-14 02:28:02', '2020-09-14 02:28:02', NULL),
(61, 'Dr.', 'Dicta quibusdam molestiae ratione aperiam. Doloremque et error incidunt beatae. Cupiditate ut beatae et pariatur praesentium in praesentium fugiat. Blanditiis quasi quis voluptas ut at omnis quia.', 'Library Technician', '2020-09-14 02:28:02', '2020-09-14 02:28:02', NULL),
(62, 'Miss', 'Quia sapiente amet magni est modi assumenda. Consequatur sint quod libero est reprehenderit doloribus quos. Odio natus alias sit esse voluptatem alias eveniet.', 'Travel Clerk', '2020-09-14 02:28:02', '2020-09-14 02:28:02', NULL),
(63, 'Mrs.', 'Est et dolorum aperiam est. Perferendis vero voluptatum magnam soluta quia omnis libero. Quibusdam vel voluptate quasi voluptas similique dolorum culpa.', 'Textile Machine Operator', '2020-09-14 02:28:02', '2020-09-14 02:28:02', NULL),
(64, 'Dr.', 'Ipsum vero perspiciatis eos tenetur et a. Sapiente reprehenderit dolor commodi consequatur doloremque dicta. Quo illum id et et voluptatibus.', 'Psychology Teacher', '2020-09-14 02:28:02', '2020-09-14 02:28:02', NULL),
(65, 'Ms.', 'Ut soluta commodi voluptas eius dolor. Unde quasi dolorum atque soluta enim. Quia autem velit fugit tenetur corporis nam et. Impedit assumenda explicabo placeat delectus occaecati aliquid officiis.', 'Public Relations Specialist', '2020-09-14 02:28:02', '2020-09-14 02:28:02', NULL),
(66, 'Miss', 'Minima excepturi quia enim nulla maiores facilis officia. Fugit molestias asperiores omnis quas quas. Non sint et nobis enim neque.', 'Bridge Tender OR Lock Tender', '2020-09-14 02:28:02', '2020-09-14 02:28:02', NULL),
(67, 'Prof.', 'Aperiam consequatur quia quo minima placeat ipsa. Rerum et quo molestiae adipisci ut. Omnis ex sed sint ab. Eum voluptas dolores nisi.', 'House Cleaner', '2020-09-14 02:28:02', '2020-09-14 02:28:02', NULL),
(68, 'Miss', 'Suscipit sit sint rem nihil velit perspiciatis consequatur quo. Corrupti rem eveniet ratione vel animi deleniti id ex. Alias labore accusantium labore eos perferendis.', 'Appliance Repairer', '2020-09-14 02:28:02', '2020-09-14 02:28:02', NULL),
(69, 'Mrs.', 'Sit aperiam labore repudiandae repellat. Aut impedit iure odio temporibus aliquam animi eos. Ut est placeat sequi quia.', 'Mining Engineer OR Geological Engineer', '2020-09-14 02:28:03', '2020-09-14 02:28:03', NULL),
(70, 'Dr.', 'Nemo modi voluptate autem natus ut facere distinctio magnam. Facere id consequatur rerum debitis. Et quaerat perspiciatis qui accusantium omnis. Pariatur voluptatem consequatur hic sed neque officia.', 'Geography Teacher', '2020-09-14 02:28:03', '2020-09-14 02:28:03', NULL),
(71, 'Prof.', 'Esse cumque nobis ipsa dicta. Nihil nam ut totam dolorem rerum aut. Eius ut sit dolor. Iusto fuga non ut in repellendus.', 'Electrical and Electronic Inspector and Tester', '2020-09-14 02:28:03', '2020-09-14 02:28:03', NULL),
(72, 'Dr.', 'Occaecati voluptas assumenda possimus. Quae eos maxime eum suscipit enim nihil ut.', 'Fish Game Warden', '2020-09-14 02:28:03', '2020-09-14 02:28:03', NULL),
(73, 'Dr.', 'Rerum vel molestiae pariatur non repellat voluptatem possimus. Commodi architecto omnis non sit possimus enim qui. Praesentium sed ipsum esse qui aliquam aut cupiditate.', 'Air Traffic Controller', '2020-09-14 02:28:03', '2020-09-14 02:28:03', NULL),
(74, 'Dr.', 'Non ratione facilis voluptatem qui. Quo et provident maxime quos impedit facilis natus.', 'Underground Mining', '2020-09-14 02:28:03', '2020-09-14 02:28:03', NULL),
(75, 'Mr.', 'Sit est consectetur est et mollitia ut expedita quos. Alias sunt perferendis enim est cumque nemo tempora quo. Dolore et provident exercitationem nihil neque. Enim accusantium fugit debitis eum.', 'Highway Maintenance Worker', '2020-09-14 02:28:03', '2020-09-14 02:28:03', NULL),
(76, 'Dr.', 'Sunt possimus eos veniam sunt facilis. Ut voluptatibus ex fugiat eum mollitia. Quis ipsa quis ullam soluta fugit eligendi exercitationem. Nemo rerum vel autem natus qui alias voluptas.', 'Aircraft Mechanics OR Aircraft Service Technician', '2020-09-14 02:28:03', '2020-09-14 02:28:03', NULL),
(77, 'Prof.', 'Alias eaque optio ut et et est. Incidunt et voluptate enim omnis. Facere excepturi qui qui molestiae officia. Et iure a laboriosam excepturi omnis aperiam sunt.', 'Pharmaceutical Sales Representative', '2020-09-14 02:28:03', '2020-09-14 02:28:03', NULL),
(78, 'Dr.', 'Tempora porro veritatis reiciendis nulla. Inventore fugit dignissimos veritatis animi perferendis. Dolor laboriosam velit quibusdam et autem asperiores.', 'Electronic Equipment Assembler', '2020-09-14 02:28:03', '2020-09-14 02:28:03', NULL),
(79, 'Miss', 'Modi accusamus autem nulla id sint consectetur sit. Sit unde aut eos et id laboriosam dolores. Dicta id corrupti vero magni eligendi ex.', 'Air Crew Member', '2020-09-14 02:28:03', '2020-09-14 02:28:03', NULL),
(80, 'Dr.', 'Blanditiis nostrum qui facere omnis. Tenetur nam ipsam voluptatem deleniti sunt. Saepe repellat numquam qui quia id laborum.', 'Social Scientists', '2020-09-14 02:28:03', '2020-09-14 02:28:03', NULL),
(81, 'Dr.', 'Earum hic qui illo amet porro voluptas. Nihil labore qui est qui. Corporis culpa totam et ducimus quis.', 'Underground Mining', '2020-09-14 02:28:03', '2020-09-14 02:28:03', NULL),
(82, 'Mrs.', 'Ea temporibus voluptas possimus odit voluptatibus error ex. A hic expedita omnis est id voluptas. Et nam est autem quo consequuntur illum ut.', 'Biological Scientist', '2020-09-14 02:28:03', '2020-09-14 02:28:03', NULL),
(83, 'Mr.', 'Non et velit quis perspiciatis similique sed a suscipit. Consequatur est nulla voluptatem magnam et. Et voluptatem non ut magni soluta. Dignissimos tempora quasi iusto est.', 'Physicist', '2020-09-14 02:28:03', '2020-09-14 02:28:03', NULL),
(84, 'Mrs.', 'Inventore aut molestias libero cum ipsam. Odio mollitia aut labore ex quos. Sit ad iusto nihil aut dignissimos repellat. Quasi qui omnis distinctio molestiae quisquam dicta hic dolorem.', 'Anthropologist', '2020-09-14 02:28:03', '2020-09-14 02:28:03', NULL),
(85, 'Dr.', 'Officia voluptatem aut aspernatur et expedita. Accusantium fuga illo voluptates. Qui minima alias porro neque.', 'Patrol Officer', '2020-09-14 02:28:03', '2020-09-14 02:28:03', NULL),
(86, 'Prof.', 'Consectetur et molestias atque est voluptatem. Corrupti tempore est culpa non exercitationem nemo ex. Commodi praesentium illo autem ducimus dignissimos sit suscipit. Qui similique provident dolorum aut.', 'Library Technician', '2020-09-14 02:28:03', '2020-09-14 02:28:03', NULL),
(87, 'Prof.', 'Excepturi nihil pariatur harum architecto sint est aliquid. Hic tenetur magni vel quia sapiente voluptatem. Reprehenderit dolor maxime officiis ut distinctio sit. Quia dolore quia placeat suscipit. Expedita aut illo aliquid ullam totam adipisci minima.', 'Etcher and Engraver', '2020-09-14 02:28:03', '2020-09-14 02:28:03', NULL),
(88, 'Dr.', 'Tempore et repellat accusantium ut temporibus error quod. Ab pariatur quod deleniti ducimus perspiciatis quos. Sequi debitis provident ut id.', 'Epidemiologist', '2020-09-14 02:28:03', '2020-09-14 02:28:03', NULL),
(89, 'Dr.', 'Dolorum facilis amet nihil. Maxime maiores neque at quam ducimus dolorem quis. Id vero nisi voluptas totam dolorum quae.', 'Refrigeration Mechanic', '2020-09-14 02:28:03', '2020-09-14 02:28:03', NULL),
(90, 'Prof.', 'Nihil libero omnis cupiditate nemo consequuntur. Ut nulla magnam illum et. Corrupti sunt omnis labore vitae molestias quas.', 'TSA', '2020-09-14 02:28:03', '2020-09-14 02:28:03', NULL),
(91, 'Dr.', 'Vel magnam sint dolorem qui reiciendis qui. Eveniet dignissimos optio aut quidem est. Expedita odio et rem laborum. Omnis consequatur et totam eius quasi sunt.', 'Mental Health Counselor', '2020-09-14 02:28:03', '2020-09-14 02:28:03', NULL),
(92, 'Dr.', 'Libero itaque autem nisi. At saepe dolor dolore ipsa debitis. Necessitatibus aliquid officiis praesentium est magni et.', 'Secondary School Teacher', '2020-09-14 02:28:03', '2020-09-14 02:28:03', NULL),
(93, 'Prof.', 'Quaerat eos sint repellendus ipsum ipsa. Et provident illum voluptas mollitia. Est ipsum est harum. Atque tempore eum laborum consequatur ducimus.', 'Private Household Cook', '2020-09-14 02:28:04', '2020-09-14 02:28:04', NULL),
(94, 'Dr.', 'Et ut id deleniti id est aspernatur nihil. Explicabo eveniet eligendi voluptates velit facere molestias. Quos rerum reprehenderit minima accusamus repudiandae.', 'Communications Teacher', '2020-09-14 02:28:04', '2020-09-14 02:28:04', NULL),
(95, 'Prof.', 'Id voluptas sequi eum eius suscipit sed qui praesentium. Reprehenderit ipsum quia maiores officiis corporis voluptas.', 'Highway Patrol Pilot', '2020-09-14 02:28:04', '2020-09-14 02:28:04', NULL),
(96, 'Prof.', 'Commodi molestiae quas in. Magni sunt sit error laudantium dolores cupiditate aut. Exercitationem minima amet cum in animi sit. Quod voluptate dolores impedit laudantium distinctio. Modi esse occaecati est non.', 'Building Inspector', '2020-09-14 02:28:04', '2020-09-14 02:28:04', NULL),
(97, 'Mr.', 'Ab esse eum sint et inventore voluptas. Eius possimus quis ut tempora vel architecto. At incidunt sed autem veniam optio et cumque. Ipsa voluptatem rerum optio necessitatibus.', 'Food Tobacco Roasting', '2020-09-14 02:28:04', '2020-09-14 02:28:04', NULL),
(98, 'Miss', 'Quia ab tempora aut at veritatis qui. Id ullam temporibus corporis. Consectetur dolore eos soluta totam veritatis sed quos. Veniam nihil quos aut dolor.', 'Makeup Artists', '2020-09-14 02:28:04', '2020-09-14 02:28:04', NULL),
(99, 'Mr.', 'Ipsum ipsam nihil et aut nostrum maiores occaecati. Est eos assumenda nobis nam magnam doloribus. Ex distinctio nihil aut dolores quas cum.', 'Administrative Services Manager', '2020-09-14 02:28:04', '2020-09-14 02:28:04', NULL),
(100, 'Dr.', 'Et quisquam voluptatibus molestias maxime. Provident error doloribus dolor dolorum neque rerum asperiores. Voluptatum facere explicabo nobis accusamus est cumque odio alias.', 'Landscaper', '2020-09-14 02:28:04', '2020-09-14 02:28:04', NULL),
(101, 'Dr.', 'Dicta quis porro mollitia dolores esse quasi mollitia earum. Quae architecto consectetur omnis possimus facilis distinctio. Corporis consequatur quo labore aut. Nobis et deserunt ipsam. Nulla et amet nesciunt quod.', 'Postal Service Clerk', '2020-09-14 02:28:04', '2020-09-14 02:28:04', NULL),
(102, 'Dr.', 'Nihil corrupti enim hic non magni tenetur nulla. Odit voluptatibus ratione at adipisci ut excepturi. Maxime ut autem cumque non ex necessitatibus nulla.', 'Health Practitioner', '2020-09-14 02:28:04', '2020-09-14 02:28:04', NULL),
(103, 'Ms.', 'Earum similique nobis vitae quia. Recusandae voluptates quas deserunt et et reiciendis. Officiis vel facere at ut placeat ipsa quis.', 'Pharmacy Technician', '2020-09-14 02:28:04', '2020-09-14 02:28:04', NULL),
(104, 'Prof.', 'Quae quam ea voluptatibus sed. Est harum ea consectetur est maxime et ut. Quos ducimus consequatur iste et aut beatae. Nam ipsum ipsum ullam occaecati aut et facilis.', 'Logistician', '2020-09-14 02:28:04', '2020-09-14 02:28:04', NULL),
(105, 'Prof.', 'Et perspiciatis voluptate officia rem eligendi magni rem vel. Voluptatem esse quis voluptatem saepe debitis doloremque. Veniam placeat in in deleniti sunt expedita. Id reprehenderit porro voluptatibus sunt.', 'Environmental Compliance Inspector', '2020-09-14 02:28:04', '2020-09-14 02:28:04', NULL),
(106, 'Ms.', 'Dolorem itaque ducimus aut minima. Vitae sed dolor totam sunt. Libero quos voluptatem quam excepturi et labore ut. Expedita odio et libero eveniet.', 'Agricultural Science Technician', '2020-09-14 02:28:04', '2020-09-14 02:28:04', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_09_14_073144_create_blog_table', 1),
(5, '2020_09_14_074012_add_category_in_blog_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
