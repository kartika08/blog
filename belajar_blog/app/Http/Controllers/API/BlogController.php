<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Blog as Model;
use App\Http\Resources\BlogResource as Resource;

class BlogController extends Controller
{
    /**
     * Display a listing of the resorce.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	return Resource::collection(Model::all());
    }

    /**
     * storea newly created resource in storage.
     *
     *	@param \Illuminate\Http\Request
     *	@return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	//
    }
}
